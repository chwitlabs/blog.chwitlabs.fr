.. title: Build Python Click Like Command-line Applications With Argparse Part 1
.. slug: build-python-click-like-command-line-applications-with-argparse-part-1
.. date: 2020-02-11 07:00:00 UTC+01:00
.. tags: python, commandline, argparse
.. category: 
.. link: 
.. description: How to build Python command-line applications with argparse but
   with the decorator style of Click.
.. type: text
.. lang: en

`Click`_ is a popular package to create Python command-Line applications.
And it's for a good reason.
Using decorators keeps the command-line configuration next to the handling code.
It is easier to relate configuration with handling code.

The argparse experience can be improved to gain Click like decorator based
configuration.

The Simple command case
=======================

Let's start with a single command build script with a few options:

- a `source` positional argument to specify the build source
- a `--output` option to specify the build result directory
- a `--clean` flag to cleanup things before building
- a repeatable `--verbose` flag to increase the verbosity level

Using argparse the classic way
------------------------------

When using argparse the classic way, arguments are defined in a separate
location from where they will be used:

.. listing:: build-python-click-like-command-line-applications-with-argparse/build_argparse_classic.py python

The generated help looks like:

.. listing:: build-python-click-like-command-line-applications-with-argparse/build_argparse_help.txt

Using Click
-----------

When using click, arguments are defined where they will be used:

.. listing:: build-python-click-like-command-line-applications-with-argparse/build_click.py python

Using argparse the Click way
----------------------------

We can have something similar using argparse:

.. listing:: build-python-click-like-command-line-applications-with-argparse/build_argparse_decorators.py python

At first build the `main()` instead of just calling `build()` might seems strange.
However, it has its advantages:

- main can be called with command-line args `main("./src", "--clean")`
- build is not modified and its call matches its signature: `build(Path("./src"), clean=True).`

Those are valuablbe interfaces when testing or when using the script as a
library.

How does it works
~~~~~~~~~~~~~~~~~

`add_argument` creates `build.__argparse_annotations__` if it does not exists
and appends information in it for later use.

Then `get_main` reads `build.__argparse_annotations__` to build an
`argparse.ArgumentParser` and build the main function

Full listing:

.. listing:: build-python-click-like-command-line-applications-with-argparse/argparse_decorators.py python

In part 2 we will see:
----------------------

- How to fallback to classic argparse if needed
  without loosing all the benefits from décorators

- How to extend to simplify some parts (like a better `--verbose` declaration).

.. _`Click`: https://pypi.org/project/click/
