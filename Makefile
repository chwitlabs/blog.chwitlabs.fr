build:
	nikola build

clean:
	nikola clean
	rm -rf output
	find . -name '.mypy_cache' -exec rm -fr {} +
	find . -name '.pytest_cache' -exec rm -fr {} +
	find . -name '__pycache__' -exec rm -fr {} +

publish: build
	scp -r output/* antoine.cezar.fr:/srv/http/blog.chwitlabs.fr/
