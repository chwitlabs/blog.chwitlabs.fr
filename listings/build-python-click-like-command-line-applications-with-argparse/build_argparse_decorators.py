from pathlib import Path

from argparse_decorators import add_argument as arg
from argparse_decorators import get_main


@arg("source", default="./src", type=Path, help="Source directory.")
@arg("--output", "-o", default="./build", type=Path, help="Build directory.")
@arg("--clean", "-c", action="store_true", help="Clean before build.")
@arg(
    "--verbose",
    "-v",
    action="count",
    default=0,
    dest="verbosity",
    help="Add more verbosity.",
)
def build(source, output, clean, verbosity):
    """Build Stuff."""
    print("Build:")
    print(f"  - source: {source}")
    print(f"  - output: {output}")
    print(f"  - clean: {clean}")
    print(f"  - verbosity: {verbosity}")


main = get_main(build)


if __name__ == "__main__":
    main()
