from pathlib import Path

import click


@click.command()
@click.argument("source", default="./src", type=Path, help="Source directory.")
@click.option("--output", "-o", default="./build", type=Path, help="Build directory.")
@click.option("--clean", "-c", is_flag=True, help="Clean before build.")
@click.option("--verbose", "-v", "verbosity", count=True, help="Add more verbosity.")
def build(source, output, clean, verbosity):
    """Build Stuff."""
    print("Build:")
    print(f"  - source: {source}")
    print(f"  - output: {output}")
    print(f"  - clean: {clean}")
    print(f"  - verbosity: {verbosity}")


if __name__ == "__main__":
    build()
