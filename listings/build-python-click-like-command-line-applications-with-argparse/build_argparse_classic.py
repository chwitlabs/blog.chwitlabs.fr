import argparse
from pathlib import Path


def parse_args(args=None):
    parser = argparse.ArgumentParser(description=build.__doc__)

    parser.add_argument("source", default="./src", type=Path, help="Source directory.")

    parser.add_argument(
        "--output", "-o", default="./build", type=Path, help="Build directory."
    )

    parser.add_argument(
        "--clean", "-c", action="store_true", help="Clean before build."
    )

    parser.add_argument(
        "--verbose",
        "-v",
        action="count",
        default=0,
        dest="verbosity",
        help="Add more verbosity.",
    )

    return parser.parse_args(args)


def build(source, output, clean, verbosity):
    """Build Stuff."""
    print("Build:")
    print(f"  - source: {source}")
    print(f"  - output: {output}")
    print(f"  - clean: {clean}")
    print(f"  - verbosity: {verbosity}")


def main(args=None):
    args = parse_args(args)
    build(args.source, args.output, args.clean, args.verbosity)


if __name__ == "__main__":
    main()
